FROM node:6.9.1

RUN useradd --user-group --create-home --shell /bin/false nodeuser \
    && npm install -g raml2html \
    && mkdir /raml && chown nodeuser:nodeuser /raml
USER nodeuser
WORKDIR /raml
CMD ["raml2html",  "api.raml > index.html"]