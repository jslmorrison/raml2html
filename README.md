#Usage

So you are using RAML to describe your API and want to generate static html docs from it, but you dont want to install Node locally.
No problem.

You can use this container to generate the api docs as follows:
```
* docker pull jslmorrison/raml2html
* docker run --rm -v $(pwd):/raml jslmorrison/raml2html raml2html api.raml > index.html
```
